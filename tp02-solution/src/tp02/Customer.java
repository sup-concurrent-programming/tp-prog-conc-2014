package tp02; // Customer.java / SUP / 2014-03-06

/**
 * Copyright 2014 Jacques Supcik,
 * College of Engineering and Architecture, Fribourg
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Simulation of a saving account
 * 
 * @author  Jacques Supcik <jacques.supcik@hefr.ch>
 * @version 1.2
 * 
 */

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class Customer extends Thread {
    
    private final static int MIN_SLEEP = 500;
    private final static int MAX_SLEEP = 2000;
    private final static int MIN_AMOUNT = -100;
    private final static int MAX_AMOUNT = 100;
    
    int id;
    
    public Customer(int id) {
        this.id = id;
    }
    
    private void deposit(int amount) throws InterruptedException {
    	SavingAccount.mutex.acquire();
        System.out.format("Customer %d deposit %d\n", id, amount);
        SavingAccount.sum += amount;
        System.out.format("New balance: %d\n", SavingAccount.sum);
        while (SavingAccount.waitingQueue.hasQueuedThreads()) {
        	SavingAccount.waitingQueue.release();
        }
        SavingAccount.mutex.release();
    }
    
    private void withdraw(int amount) throws InterruptedException {
    	SavingAccount.mutex.acquire();
        System.out.format("Customer %d want to withdraw %d\n", id, amount);
        long t0 = System.currentTimeMillis();
        while (SavingAccount.sum - amount < 0) {
        	SavingAccount.mutex.release();
			if (!SavingAccount.waitingQueue.tryAcquire(1000,
					TimeUnit.MILLISECONDS)) {
				if (System.currentTimeMillis() - t0 > 20000) {
					System.out.format("Customer %d giving up\n", id);
					return;
				}
			}
        	SavingAccount.mutex.acquire();
        }
        SavingAccount.sum -= amount;
        System.out.format("Customer %d withdraw %d\n", id, amount);
        System.out.format("New balance: %d\n", SavingAccount.sum);
        SavingAccount.mutex.release();
    }

    @Override
    public void run() {
        while (true) {
            try {
				sleep(ThreadLocalRandom.current().nextInt(MIN_SLEEP, MAX_SLEEP));
				int amount = ThreadLocalRandom.current().nextInt(MIN_AMOUNT,
						MAX_AMOUNT);
                if (amount < 0) {
                    withdraw(-amount);
                } else if (amount > 0) {
                    deposit(amount);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
