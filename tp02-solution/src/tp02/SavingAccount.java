package tp02; // SavingAccount.java / SUP / 2014-03-06

/**
 * Copyright 2014 Jacques Supcik,
 * College of Engineering and Architecture, Fribourg
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Simulation of a saving account
 * 
 * @author  Jacques Supcik <jacques.supcik@hefr.ch>
 * @version 1.2
 * 
 */

import java.util.concurrent.Semaphore;

public class SavingAccount {
    
	static final int N_OF_CUSTOMERS = 5;
    static long sum = 0;
    static Semaphore mutex = new Semaphore(1);
    static Semaphore waitingQueue = new Semaphore(0, true);
    
    public static void main(String[] args) {
        for (int i = 0; i < N_OF_CUSTOMERS; i++) {
            Thread t = new Customer(i);
            t.start();
        }
    }

}
