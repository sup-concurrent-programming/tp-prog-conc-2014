__author__ = 'Jacques Supcik'

import multiprocessing

def zero(n, west):
    for i in range(n):
        west.put(0)

def sink(n, north):
    for i in range(n):
        north.get()

def result(n, row, east):
    for i in range(n):
        x = east.get()
        print("[{0}, {1}] = {2}".format(i, row.value, x))

def source(v, south):
    for i in v:
        south.put(i)

def multiply (n, x, north, east, south, west):
    for i in range(n):
        y = north.get()
        sum = east.get()
        sum = sum + x * y
        south.put(y)
        west.put(sum)

workers = list()
def do(target, args):
    global workers
    p = multiprocessing.Process(target=target, args=args)
    workers.append(p)

if __name__ == '__main__':
    m = multiprocessing.Manager()
    SIZE = 3
    MATRIX_A = [[3,2,1], [6,5,4], [9,8,7]]
    MATRIX_B = [[1,0,0], [0,1,2], [1,0,2]]

    south = [[m.Queue() for x in range(SIZE+1)] for x in range(SIZE)]
    west = [[m.Queue() for x in range(SIZE+1)] for x in range(SIZE)]
    val = [m.Array('i',x) for x in [[1,0,0], [0,1,2], [1,0,2]]]

    for i in range(SIZE):
        do(source, (val[i], south[i][0]))
        do(sink, (SIZE, south[i][SIZE]))
        do(zero, (SIZE, west[i][0]))
        do(result, (SIZE, m.Value('i', i), west[i][SIZE]))

    for i in range(SIZE):
        for j in range(SIZE):
            do (multiply, (
                SIZE, MATRIX_A[j][i],
                south[i][j], west[j][i], south[i][j+1], west[j][i+1]
            ))

    for i in workers:
        i.start()

    for i in workers:
        i.join()