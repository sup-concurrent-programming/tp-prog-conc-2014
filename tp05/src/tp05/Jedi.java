package tp05;

import static tp05.Spacecraft.*;

public class Jedi extends Thread {

    // you might need additional fields here
    private String name;
    
    public Jedi(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            // ...
            jedi++;
            // ...
            enterTerminal(this);
            // ...
            boardSpacecraft(this);
            barrier.await();
            // ...
            if // ...
                // ...
                flySpacecraft(this);
                // ...
            }
            // ...
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public String toString() {
        return String.format("Jedi %s", name);
    }
}
