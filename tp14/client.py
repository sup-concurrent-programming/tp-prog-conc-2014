__author__ = 'Jacques Supcik'

import multiprocessing
import sys
from multiprocessing.managers import BaseManager

PASSWORD = b'abracadabnra'
HOST = "cruncher.tic.eia-fr.ch"

PORT = 50000
ID = "Jacques"

NOT_FOUND = 0
FOUND = 1
INFO = 2
DEBUG = 3


class QueueManager(BaseManager): pass


QueueManager.register('job_queue', callable=lambda: job_queue)
QueueManager.register('result_queue', callable=lambda: result_queue)


def main():
    try:
        m = QueueManager(address=(HOST, PORT), authkey=PASSWORD)
        m.connect()
    except Exception as e:
        print(e)
        sys.exit(1)


if __name__ == '__main__':
    main()