__author__ = 'Jacques Supcik'

import sys
from multiprocessing import Process, Queue
from multiprocessing.managers import BaseManager

JOB_QUEUE_SIZE = 10
PASSWORD = b'abracadabnra'
PORT = 50000
JOB_SIZE = 10000000  # 10 Millions!
CHALLENGE = 1000329093644473695673
# CHALLENGE = 45156706475718931

NOT_FOUND = 0;
FOUND = 1;
INFO = 2;
DEBUG = 3


def isqrt(n):  # Newton's method
    x = n
    y = (x + n // x) // 2
    while y < x:
        x = y
        y = (x + n // x) // 2
    return x


class ResultPrinter(Process):
    def __init__(self, q):
        self.q = q
        Process.__init__(self)
        print("Result Printer Ready")

    def run(self):
        while True:
            res = self.q.get()
            if res[2] == NOT_FOUND:
                self.job_done_queue.put(res[0])
            elif (res[2] == FOUND):
                if (res[4] > 1 and res[4] < CHALLENGE
                    and CHALLENGE % res[4] == 0):
                    print("FOUND!")
                    self.job_done_queue.put(res[0])
                    break
                else:
                    print("Cheater!")
            else:
                print("> {0}".format(res))


class WorkProvider(Process):
    def __init__(self, q, n):
        self.q = q
        self.n = n
        Process.__init__(self)
        print("Work Provider Ready")

    def run(self):
        limit = isqrt(self.n) + 1
        step = (limit - 3) // JOB_SIZE
        if step < 2:
            step = 2  # ensures that step_size is >= 2
        elif step % 2 == 1:
            step += 1  # ensures that step_size is even

        limit = 3 + (limit // step + 1) * step
        job_id = 0
        for i in range(step // 2):
            t = (job_id, self.n, 3 + i * 2, step, limit + i * 2)
            self.q.put(t)
            job_id += 1
        print("DONE")


def main():
    job_queue = Queue(JOB_QUEUE_SIZE)
    result_queue = Queue()

    p = ResultPrinter(result_queue)
    p.start()

    if CHALLENGE % 2 == 0:
        result_queue.put((-1, 'ROOT', FOUND, CHALLENGE, 2))
        sys.exit()

    w = WorkProvider(job_queue, CHALLENGE)
    w.start()

    class QueueManager(BaseManager): pass

    QueueManager.register('job_queue', callable=lambda: job_queue)
    QueueManager.register('result_queue', callable=lambda: result_queue)

    m = QueueManager(address=('', PORT), authkey=PASSWORD)
    s = m.get_server()

    print("Server Ready")
    s.serve_forever()


if __name__ == '__main__':
    main()