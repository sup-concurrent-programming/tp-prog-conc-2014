__author__ = 'You'

import multiprocessing
import time
import random

PUT_INGREDIENT_TIME = (1, 2)
MAKE_SANDWICH_TIME = (3, 5)
EAT_SANDWICH_TIME = (3, 5)
RECOVER_TIME = (3, 5)

now = time.time()


def time_print(s):
    print("{0:7.2f}: {1}".format(time.time() - now, s))


def put_ingredient(ingredient, ...):
    time_print("Grocer puts {0}".format(ingredient))
    ...
    time.sleep(random.randrange(*PUT_INGREDIENT_TIME))


def make_sandwich(name, ingredient):
    time_print("{0} adds {1} and makes his sandwich".format(name, ingredient))
    time.sleep(random.randrange(*MAKE_SANDWICH_TIME))


def eat(name):
    time_print("{0} eats his sandwich".format(name))
    time.sleep(random.randrange(*EAT_SANDWICH_TIME))
    time_print("{0} finished".format(name))


def grocer(...):
    while True:
        ...
        x = random.randrange(3)
        y = random.randrange(2)
        if x == 0:
            put_ingredient("ham", ham)
            if y == 0: put_ingredient("butter", butter)
            else: put_ingredient("bread", bread)
        elif x == 1:
            put_ingredient("bread", bread)
            if y == 0: put_ingredient("butter", butter)
            else: put_ingredient("ham", ham)
        else:
            put_ingredient("butter", butter)
            if y == 0: put_ingredient("bread", bread)
            else: put_ingredient("ham", ham)

...

def gourmet(name, ingredient, ...):
    while True:
        ...
        make_sandwich(name, ingredient)
        ...
        eat(name)
        time.sleep(random.randrange(*RECOVER_TIME))


def main():


    grocer_process = multiprocessing.Process(
        target=grocer, args=(...)
    )
    grocer_process.start()

    for (name, ingredient, ...) in [
        ("Baker", "bread", ...),
        ("Milkman", "butter", ...),
        ("Butcher", "ham", ...)
    ]:
        p = multiprocessing.Process(
            target=gourmet, args=(name, ingredient, ...)
        )
        p.start()

    ....

    grocer_process.join()


if __name__ == '__main__':
    main()