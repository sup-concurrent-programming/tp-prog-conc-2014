__author__ = 'You'

import multiprocessing
import time
import random

NB_OF_SAVAGES = 20
POT_SIZE = 8
EAT_TIME = (5, 10)
COOK_TIME = (3, 10)
WAIT_TIME = (6, 30)

now = time.time()


def time_print(s):
    print("{0:7.2f}: {1}".format(time.time() - now, s))


def put_serving_in_pot(n):
    time_print("Cook start putting {0} servings in pot".format(n))
    time.sleep(random.randrange(*COOK_TIME))
    time_print("Cook finished")


def hungry(id):
    time_print("Savage {0} is hungry".format(id))


def eat(id):
    time_print("Savage {0} eats".format(id))
    time.sleep(random.randrange(*EAT_TIME))


def get_serving_from_pot(id, remaining):
    time_print(
        "Savage {0} gets 1 serving from pot (remaining={1})".format(
            id, remaining
        )
    )


def savage(id, ...):
    while True:
        time.sleep(random.randrange(*WAIT_TIME))
        hungry(id)
        ...
        get_serving_from_pot(id, ...)
        ...
        eat(id)


def cook(...):
    time_print("Cook is ready")
    while True:
        ...
        put_serving_in_pot(POT_SIZE)
        ...


def main():
    ...

    cook_process = multiprocessing.Process(target=cook, args=(...))
    cook_process.start()
    for i in range(NB_OF_SAVAGES):
        p = multiprocessing.Process(target=savage, args=(...))
        p.start()
        
    cook_process.join()


if __name__ == '__main__':
    main()