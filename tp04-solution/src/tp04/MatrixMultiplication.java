package tp04;

import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

public class MatrixMultiplication {

    static final boolean VERBOSE = false;
    static final int SIZE = 2000;
    static final int CONSUMERS = 8;
    static final int BUFFER_SIZE = 10;
    static Random random = new Random(0);

    static ArrayBlockingQueue<Coordinate> buffer = new ArrayBlockingQueue<Coordinate>(
            BUFFER_SIZE);

    static double[][] a;
    static double[][] b;
    static double[][] c;

    public static void print(double[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.printf("%7.2f", a[i][j]);
                if (j < a[i].length - 1)
                    System.out.print(", ");
            }
            System.out.println();
        }
    }

    public static double[][] randomMatrix(int size) {
        double[][] result = new double[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                result[i][j] = random.nextDouble() * 10.0 - 5.0;
            }
        }
        return result;
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t;

        a = randomMatrix(SIZE);
        b = randomMatrix(SIZE);

        long t0 = System.nanoTime();
        int h = a.length;
        int w = b[0].length;
        c = new double[h][w];
        LinkedList<Thread> workers = new LinkedList<Thread>();
        t = new Producer(h, w);
        t.start();
        workers.add(t);
        for (int i = 0; i < CONSUMERS; i++) {
            t = new Consumer(i);
            t.start();
            workers.add(t);
        }
        while (!workers.isEmpty()) {
            t = workers.remove();
            t.join();
        }

        if (VERBOSE)
            print(c);

        System.out.println();
        System.out.printf("Done in %.2e seconds",
                (double) (System.nanoTime() - t0) / 1_000_000_000);
        System.out.println();
    }
}
