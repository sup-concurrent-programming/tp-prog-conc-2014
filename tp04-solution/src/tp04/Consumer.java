package tp04;

public class Consumer extends Thread {

    int id;

    public Consumer(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        System.out.printf("%s started\n", this);
        Coordinate c;
        try {
            while (true) {
                c = MatrixMultiplication.buffer.take();
                if (c.x < 0) {
                    MatrixMultiplication.buffer.put(c);
                    return;
                }
                if (MatrixMultiplication.VERBOSE)
                    System.out.printf("%s consumes %s\n", this, c);
                MatrixMultiplication.c[c.y][c.x] = 0;
                for (int i = 0; i < MatrixMultiplication.b.length; i++) {
                    MatrixMultiplication.c[c.y][c.x] += MatrixMultiplication.a[c.y][i]
                            * MatrixMultiplication.b[i][c.x];
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("%s finished\n", this);
    }

    @Override
    public String toString() {
        return String.format("Consumer %d", id);
    }

}
