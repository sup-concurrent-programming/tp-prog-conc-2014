package tp04;

import java.util.ArrayList;

public class Producer extends Thread {

    int h;
    int w;

    public Producer(int h, int w) {
        this.h = h;
        this.w = w;
    }

    @Override
    public void run() {
        System.out.printf("%s started\n", this);
        // Produce Coordinates
        ArrayList<Coordinate> coordinates = new ArrayList<Coordinate>();
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                coordinates.add(new Coordinate(x, y));
            }
        }
        coordinates.add(new Coordinate(-1, -1)); // Sentinell
        try {
            for (Coordinate c : coordinates) {
                if (MatrixMultiplication.VERBOSE)
                    System.out.printf("%s produces %s\n", this, c);
                MatrixMultiplication.buffer.put(c);
            }
            System.out.printf("%s finished\n", this);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Producer";
    }

}
