package tp05;

// Add header here

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class Spacecraft {

	// ----- Shared variables -----
	
	// You will probably need a barrier:
    static CyclicBarrier barrier = new CyclicBarrier(4);
    
	// and a mutex to protect shares resources:
    static Semaphore mutex = new Semaphore(1);
    
    // You might also need counters for the Jedi and the Sith:
    static int jedi = 0;
    static int sith = 0;

    // ... and you might need a couple of queues where Jedi and Sith.
    // You can implement these queues using Semaphores as well:
    static Semaphore jediQueue = new Semaphore(0);
    static Semaphore sithQueue = new Semaphore(0);
    
    static void enterTerminal(Object o) {
        System.out.printf(". %s enters the terminal\n", o);
    }
    
    static void boardSpacecraft(Object o) {
        System.out.printf("+ %s is boarding\n", o);
    }
    
    static void flySpacecraft(Object o) {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.printf("> %s is flying the spacecraft\n", o);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }
    
    public static void main(String[] args) throws InterruptedException {
        int jediIndex = 0;
        int sithIndex = 0;
        while (true) {
            Thread.sleep(500);
            if (Math.random() > 0.5) {
                new Jedi(String.format("J%04d", jediIndex++)).start();
            } else {
                new Sith(String.format("S%04d", sithIndex++)).start();
            }
        }
    }

}