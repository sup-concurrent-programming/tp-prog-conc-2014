package tp05;

import static tp05.Spacecraft.*;

public class Jedi extends Thread {

    private String name;
    private boolean isCapitain;
    
    public Jedi(String name) {
        this.name = name;
        this.isCapitain = false;
    }

    @Override
    public void run() {
        try {
            mutex.acquire();
            jedi++;
            enterTerminal(this);
            if (jedi == 4) {
            	jediQueue.release(4);
            	jedi = 0;
            	isCapitain = true;
            } else if (jedi == 2 && sith >= 2) {
            	jediQueue.release(2);
            	sithQueue.release(2);
            	jedi = 0;
            	sith -= 2;
            	isCapitain = true;
            } else {
            	mutex.release();
            	isCapitain = false;
            }
            
            jediQueue.acquire();

            boardSpacecraft(this);
            barrier.await();

            if (isCapitain) {
                flySpacecraft(this);
                mutex.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public String toString() {
        return String.format("Jedi %s", name);
    }
}
