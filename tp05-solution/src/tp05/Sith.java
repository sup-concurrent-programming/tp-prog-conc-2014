package tp05;

import static tp05.Spacecraft.*;

public class Sith extends Thread {

    private String name;
    private boolean isCapitain;
    
    public Sith(String name) {
        this.name = name;
        this.isCapitain = false;
    }

    @Override
    public void run() {
        try {
            mutex.acquire();
            sith++;
            enterTerminal(this);
            if (sith == 4) {
            	sithQueue.release(4);
            	sith = 0;
            	isCapitain = true;
            } else if (sith == 2 && jedi >= 2) {
            	sithQueue.release(2);
            	jediQueue.release(2);
            	sith = 0;
            	jedi -= 2;
            	isCapitain = true;
            } else {
            	mutex.release();
            	isCapitain = false;
            }
            
            sithQueue.acquire();

            boardSpacecraft(this);
            barrier.await();

            if (isCapitain) {
                flySpacecraft(this);
                mutex.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public String toString() {
        return String.format("Sith %s", name);
    }
}
