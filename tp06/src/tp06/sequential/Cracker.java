package tp06.sequential;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import arlut.csd.crypto.Sha512Crypt;

public class Cracker {
    
    static void crack(String challenge) throws IOException {
        System.out.printf("Searching: %s...\n", challenge); 
        File file = new File("dic-0294.txt");
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        
        String line = null;
        long t0 = System.nanoTime();
        
        while ((line = reader.readLine()) != null)  {
            if (Sha512Crypt.verifyPassword(line, challenge)) {
                long t1 = System.nanoTime();
                double dt = (double)(t1 - t0) / 1000000000;
                System.out.printf("Found: %s in %f seconds\n", line, dt);
                reader.close();
                return;
            }
        }
        System.out.println("Not found. Your password is secure");
        reader.close();
    }

    public static void main(String[] args) throws IOException {
        String challenge =
                "$6$O2p5lEMbHFc3nmyR$uR5FdRkBOFJ7UdxjkPXBQTtd7rTkSqEHa"+
                "T4qj0anHq6T60QdQBfBaRtak42LFtf5aKYHQkij7i487qTQ3KVXF0";
        crack(challenge);
        
    }

}
