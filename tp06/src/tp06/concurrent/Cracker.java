package tp06.concurrent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Cracker {
    
    static void crack(String challenge) throws IOException, InterruptedException, ExecutionException {
        int cores = Runtime.getRuntime().availableProcessors();
        System.out.printf("Running on a system with %d cores\n", cores);

        File file = new File("dic-0294.txt");
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);

        System.out.printf("Searching: %s...\n", challenge);
        String line = null;
        
        ExecutorService executor = Executors.newFixedThreadPool(cores);
        List<Future<Boolean>> list = new ArrayList<Future<Boolean>>();

        while ((line = reader.readLine()) != null) {
            // TODO Complete task submission
        }
        reader.close();
        System.out.printf("Using a dictionnary with %s passwords\n", list.size());
        
        // TODO Result evaluation
    }
        
    public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
        String challenge =
                "$6$O2p5lEMbHFc3nmyR$uR5FdRkBOFJ7UdxjkPXBQTtd7rTkSqEHa"+
                "T4qj0anHq6T60QdQBfBaRtak42LFtf5aKYHQkij7i487qTQ3KVXF0";
        crack(challenge);
    }

}
