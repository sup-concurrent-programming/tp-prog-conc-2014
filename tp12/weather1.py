#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import urllib.request

__author__ = 'Jacques Supcik'
__version__ = "0.1"


class Wunderground(object):
    def __init__(self, url='http://api.wunderground.com/api', key=None,
                 country=None, city=None):
        self.base_url = url
        self.key = key
        self.country = country
        self.city = city

    def _cmd(self, command, country=None, city=None):
        if country is None: country = self.country
        if city is None: city = self.city
        url = '{base}/{key}/{command}/q/{c}/{l}.json'.format(
            base=self.base_url, key=self.key, command=command, c=country, l=city
        )
        response = urllib.request.urlopen(url)
        content = response.read()
        return json.loads(content.decode('utf8'))

    def astronomy(self, country=None, city=None):
        return self._cmd('astronomy', country, city)


def main():
    config = json.load(open("weather.json"))
    country = "Switzerland"
    city = "Fribourg"

    w = Wunderground(key=config['key'], country=country, city=city)
    astro = w.astronomy()

    sunrise = astro['moon_phase']['sunrise']
    sunset = astro['moon_phase']['sunset']

    print("-" * 78)
    print("A {l}, le soleil se lève à {rh:02d}h{rm:02d} "
          "et se couche à {sh:02d}h{sm:02d}".format(
        l=city,
        rh=int(sunrise['hour']), rm=int(sunrise['minute']),
        sh=int(sunset['hour']), sm=int(sunset['minute']),
    ))
    print("-" * 78)


if __name__ == '__main__':
    main()