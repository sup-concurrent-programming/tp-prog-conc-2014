__author__ = 'Jacques Supcik'


import multiprocessing, time, random

NB_OF_CUSTOMERS = 20


class Customer(multiprocessing.Process):
    WAITING_TIME = (1, 10)
    RECOVERY_TIME = (20, 30)
    # TO COMPLETE

    def __init__(self, name, train):
        multiprocessing.Process.__init__(self)
        self.name = name
        self.train = train

    def run(self):
        while True:
            time.sleep(random.uniform(*self.WAITING_TIME))
            self.train.board(self)
            # TO COMPLETE
            self.train.unboard(self)
            time.sleep(random.uniform(*self.RECOVERY_TIME))

    def __str__(self):
        return self.name

class Train(multiprocessing.Process):
    CAPACITY = 8
    RUNNING_TIME = (5, 6)
    RECOVERY_TIME = 1
    # TO COMPLETE

    def __init__(self):
        multiprocessing.Process.__init__(self)
        pass
        # TO COMPLETE

    def board(self, customer):
        pass
        # TO COMPLETE


    def unboard(self, customer):
        pass
        # TO COMPLETE

    def load(self):
        pass
        # TO COMPLETE

    def unload(self):
        pass
        # TO COMPLETE

    def run(self):
        while True:
            self.load()
            time.sleep(random.uniform(*self.RUNNING_TIME))
            self.unload()
            time.sleep(self.RECOVERY_TIME)


def main():
    train = Train()
    train.start()

    for i in range(NB_OF_CUSTOMERS):
        # TO CCOMPLETE
        c = Customer(name, train)
        c.start()

    train.join()


if __name__ == '__main__':
    main()