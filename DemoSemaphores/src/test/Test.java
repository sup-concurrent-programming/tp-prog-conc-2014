package test;

import java.util.concurrent.Semaphore;

public class Test {
	
	public static Semaphore s = new Semaphore(0);
	
	public static void main(String[] args) throws InterruptedException {
		Thread p = new Consumer();
		Thread q = new Consumer();
		p.start();
		q.start();
		Thread.sleep(1000);
		s.release(3);
		Thread.sleep(1000);
		s.release(3);
		System.out.println(s.availablePermits());
		Thread.sleep(1000);
		System.out.println(s.availablePermits());
	}

}
