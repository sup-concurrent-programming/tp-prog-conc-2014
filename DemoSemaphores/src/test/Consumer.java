package test;

public class Consumer extends Thread {
	
	@Override
	public void run() {
		System.out.println("Waiting for S");
		try {
			Test.s.acquire(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Done");
	}
}
