package SB_Using_Conditions;

import java.util.Random;

public class Customer implements Runnable {

    static final int SHORT_TIME_MIN =  1000; // Short waiting time
    static final int SHORT_TIME_MAX =  3000; // in case the shop was full.
    static final int LONG_TIME_MIN  = 10000; // Normal (long) waiting time.
    static final int LONG_TIME_MAX  = 15000;
    
    String id = new String("UNDEF");
    static private Random randomGenerator = new Random();
    
    public Customer(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.id;
    }

    @Override
    public void run() {
        boolean short_time = true;
        while (true) {
            try {
                if (short_time) {
                    Thread.sleep(SHORT_TIME_MIN + 
                            randomGenerator.nextInt(SHORT_TIME_MAX - SHORT_TIME_MIN));
                } else {
                    Thread.sleep(LONG_TIME_MIN + 
                            randomGenerator.nextInt(LONG_TIME_MAX - LONG_TIME_MIN));                    
                }
                if (SleepingBarber.monitor.get_haircut(this) == 1) {
                    // Customer had her haircut, so she can wait longer
                    short_time = false;
                } else {
                    // the shop was full, so the customer will try again soon
                    short_time = true;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
