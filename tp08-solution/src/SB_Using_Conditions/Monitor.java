package SB_Using_Conditions;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Monitor {

    final Lock lock = new ReentrantLock();
    final Condition barber_sleeping  = lock.newCondition(); 
    final Condition barber_ready     = lock.newCondition(); 
    final Condition customer_ready   = lock.newCondition(); 
    final Condition barber_done      = lock.newCondition(); 
    final Condition customer_leaving = lock.newCondition(); 

    int customer_waiting = 0;
    final static int SEATS = 5;
    
    //
    // Methods called by the barber
    //
    public void get_next_customer () throws InterruptedException {
        lock.lock();
        try {
            System.out.format("Barber checks customers%n");
            while (customer_waiting <= 0) {
                System.out.format("Barber Sleeping%n");
                barber_sleeping.await();
            }
            customer_waiting--;
            System.out.format("Barber is waiting%n");
            
            // 1st Rendez-vous
            barber_ready.signal();
            customer_ready.await();
            
            System.out.format("Barber working%n");
        } finally {
            lock.unlock();
        }
    }
    
    public void finished_cut() throws InterruptedException {
        lock.lock();
        try {
            System.out.format("Barber done, waiting for customer to leave%n");
            
            // 2nd Rendez-vous
            barber_done.signal();
            customer_leaving.await();
            
        } finally {
            lock.unlock();
        }
    }
    
    //
    // Method called by the customers
    //
    public int get_haircut (Customer c) throws InterruptedException {
        lock.lock();
        try {
            System.out.format("Customer %s wants a haircut%n", c);
            if (customer_waiting < SEATS) {
                customer_waiting++;

                barber_sleeping.signal();

                System.out.format("Customer %s is waiting%n", c);

                // 1st Rendez-vous
                barber_ready.await();
                customer_ready.signal();
                
                System.out.format("Customer %s is having haircut%n", c);
                
                // 2nd Rendez-vous
                barber_done.await();
                customer_leaving.signal();
                
                System.out.format("Customer %s is finished%n", c);
                
                return 1;
            } else {
                System.out.format("Salon is full... customer %s is leaving%n", c);
                return 0;
            }

        } finally {
            lock.unlock();
        }
    }
    
}
