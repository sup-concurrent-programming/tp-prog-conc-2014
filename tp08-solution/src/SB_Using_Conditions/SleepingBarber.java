package SB_Using_Conditions;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Sleeping Barber using Locks and Conditions
 * 
 * @author Jacques Supcik <jacques.supcik@hefr.ch>
 * @version 1.0
 * @since 2012-05-17
 * 
 */

public class SleepingBarber {
    public static ExecutorService threadExecutor = Executors.newCachedThreadPool();    
    public static Monitor monitor = new Monitor();
    
    final static int N_OF_CUSTOMERS = 8;
    
    public static void main(String[] args) {
        String name;
        File inFile = new File("names.txt");
        Scanner in;
        try {
            in = new Scanner(inFile);
            Barber barber = new Barber();
            Customer customers[] = new Customer[N_OF_CUSTOMERS];
            threadExecutor.execute(barber);
            for (int i = 0; i < N_OF_CUSTOMERS; i++) {
                name = in.nextLine();
                customers[i] = new Customer(name);
                threadExecutor.execute(customers[i]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
    }
}

