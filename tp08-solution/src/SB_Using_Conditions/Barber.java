package SB_Using_Conditions;

import java.util.Random;

public class Barber implements Runnable {
    
    static final int CUT_TIME_MIN = 1000;
    static final int CUT_TIME_MAX = 2000;

    static private Random randomGenerator = new Random();
    
    @Override
    public void run() {
        while (true) {
            try {
                SleepingBarber.monitor.get_next_customer();
                Thread.sleep(CUT_TIME_MIN + randomGenerator.nextInt(CUT_TIME_MAX - CUT_TIME_MIN));
                SleepingBarber.monitor.finished_cut();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
