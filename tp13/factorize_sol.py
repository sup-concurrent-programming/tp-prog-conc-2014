__author__ = 'Jacques Supcik'

import math
import time
import multiprocessing
import concurrent.futures
import sys

data = [
    # 20879261905014453717613047007180135228912619222862481375313569207173767457683250074334689152784306953662044767167796230295986610340876056003341886587734664031563277083824559261562932596960725073158055494119587593679739968944393830260533804991189770161471276278600987477973363849043248764220289756095321172281051427823368438458965955381981048524059935455248674470899922686940781449819035248903435645477657148163128806799051799746863388098596114906914535755317894886853530925728384002446419557607008918894490531530994724015142878322358427353914916629426063885536308438539858564830784449288116318643178077851029860100557,
    12304835455086049,   # 54 bit -> 2 sec
    51763355805791737,   # 56 bit -> 4 sec
    242671686669151741,  # 58 bit -> 8 secondes
    16997662368543130327 # 64 bit -> environ 5 minutes
]

def isqrt(n): # Newton's method
    x = n
    y = (x + n // x) // 2
    while y < x:
        x = y
        y = (x + n // x) // 2
    return x

def factorize_worker(n, start, step):
    for i in range(start, 3, step):
         if n % i == 0:
             return i
    return None

def factorize(n):
    if n % 2 == 0:
        return (2)

    cpu_count = multiprocessing.cpu_count()
    sqrt_n = isqrt(n)

    # fs = list()
    # with concurrent.futures.ProcessPoolExecutor() as executor:
    #     for i in range(cpu_count):
    #         f = executor.submit(
    #             factorize_worker, n, 3 + 2 * i, 2 * cpu_count, sqrt_n + 1
    #         )
    #         fs.append(f)

    limit = isqrt(n) + 1
    if limit % 2 == 0:
        limit += 1
    step = 2 * cpu_count
    res = None
    with concurrent.futures.ProcessPoolExecutor() as executor:
        fs = [executor.submit(
                factorize_worker, n, limit - 2 * i, -step
            ) for i in range(cpu_count)]

        for i in concurrent.futures.as_completed(fs):
            if i.result() is not None:
                res = i.result()
                break

        executor.shutdown(wait=False)

    return res;

def main():
    for n in data:
        t0 = time.time()
        r = factorize(n)
        t = time.time() - t0
        if r is None:
            print ("{0} is prime!".format(n))
        else:
            print ("{0} = {1} * {2} (in {3:.2f} sec)".format(n, r, n//r, t))

    print("DONE")
    sys.exit(1)

if __name__ == '__main__':
    main()

# 12304835455086049 = 110461823 * 111394463 (in 1.63 sec)
# 51763355805791737 = 205091933 * 252390989 (in 3.34 sec)
# 242671686669151741 = 459666797 * 527929553 (in 7.76 sec)
# 16997662368543130327 = 4085792573 * 4160187299 (in 98.15 sec)