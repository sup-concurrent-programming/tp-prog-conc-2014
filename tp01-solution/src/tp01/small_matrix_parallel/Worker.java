/*
 * ===========================================================================
 * Title  : Correction of Series E01 (Matrix), Exercise 3
 * Author : Jacques Supcik <jacques [dot] supcik [at] eifr [dot] ch>
 * Date   : February 22 2013
 * File   : Worker.java
 * ===========================================================================
 * Copyright 2013 College of Engineering and Architecture, Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ===========================================================================
 */

package tp01.small_matrix_parallel;

public class Worker extends Thread {

    double[][] a;
    double[][] b;
    double[][] result;
    int x;
    int y;

    public Worker(double[][] a, double[][] b, double[][] result, int x, int y) {
        this.a = a;
        this.b = b;
        this.result = result;
        this.x = x;
        this.y = y;
    }

    public void run() {
        int w = a[0].length;
        result[x][y] = 0;
        for (int k = 0; k < w; k++) {
            result[x][y] += a[x][k] * b[k][y];
        }
    }

}
