/*
 * ===========================================================================
 * Title  : Correction of Series E01 (Matrix), Exercise 3
 * Author : Jacques Supcik <jacques [dot] supcik [at] eifr [dot] ch>
 * Date   : February 22 2013
 * File   : E01.java
 * ===========================================================================
 * Copyright 2013 College of Engineering and Architecture, Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ===========================================================================
 */

package tp01.small_matrix_sequential;

public class Matrix {

    public static void print(double[][] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				System.out.printf("%7.2f", a[i][j]);
				if (j < a[i].length - 1) System.out.print(", ");
			}
			System.out.println();
		}
	}

    public static double[][] mult(double[][] a, double[][] b) {
        int w = a[0].length;
        int h = b.length;
        double[][] result = new double[h][w];
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                result[j][i] = 0;
                for (int k = 0; k < w; k++) {
                    result[j][i] += a[j][k] * b[k][i];
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        double[][] a = new double[][] {{3, -1, 0}, {2, 5, 1}, {-7, 1, 3}};
        double[][] b = new double[][] {{6, -1, 0}, {0, 1, -2}, {3, -8, 1}};
        print(mult(a, b));
    }
}
