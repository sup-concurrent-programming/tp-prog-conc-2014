package tp01;

public class Matrix {

    public static void print(double[][] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				System.out.printf("%7.2f", a[i][j]);
				if (j < a[i].length - 1) System.out.print(", ");
			}
			System.out.println();
		}
	}

    public static double[][] mult(double[][] a, double[][] b) {
        int w = a[0].length;
        int h = b.length;
        System.out.printf("w: %d, h: %d\n", w, h);
        double[][] result = new double[h][w];
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                result[j][i] = 0;
                for (int k = 0; k < w; k++) {
                    System.out.printf("i: %d, j: %d, k:%d\n", i, j, k);
                    result[j][i] += a[j][k] * b[k][i];
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        double[][] a = new double[][] {{3, -1, 0}, {2, 5, 1}, {-7, 1, 3}};
        double[][] b = new double[][] {{6, -1, 0}, {0, 1, -2}, {3, -8, 1}};
        print(mult(a, b));
    }
}
