/*
 * ===========================================================================
 * Title  : Correction of Series E01 (Matrix), Exercise 4
 * Author : Jacques Supcik <jacques [dot] supcik [at] eifr [dot] ch>
 * Date   : February 22 2013
 * File   : Worker.java
 * ===========================================================================
 * Copyright 2013 College of Engineering and Architecture, Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ===========================================================================
 */

package tp01.big_matrix_parallel;

public class Worker extends Thread {

    double[][] a;
    double[][] b;
    double[][] result;
    int startRow;
    int step;

    public Worker(double[][] a, double[][] b, double[][] result, int startRow, int step) {
        this.a = a;
        this.b = b;
        this.result = result;
        this.startRow = startRow;
        this.step = step;
    }

    public void run() {
        int w = a[0].length;
        int h = b.length;
        for (int i = startRow; i < h; i+=step) {
            for (int j = 0; j < w; j++) {
                result[j][i] = 0;
                for (int k = 0; k < w; k++) {
                    result[j][i] += a[j][k] * b[k][i];
                }
            }
        } 
        
    }
}
