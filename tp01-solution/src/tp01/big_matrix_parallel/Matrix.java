/*
 * ===========================================================================
 * Title  : Correction of Series E01 (Matrix), Exercise 4
 * Author : Jacques Supcik <jacques [dot] supcik [at] eifr [dot] ch>
 * Date   : February 22 2013
 * File   : E01.java
 * ===========================================================================
 * Copyright 2013 College of Engineering and Architecture, Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ===========================================================================
 */

package tp01.big_matrix_parallel;

import java.util.Random;

public class Matrix {

    static Random random = new Random(0);

    public static void print(double[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.printf("%7.2f", a[i][j]);
                if (j < a[i].length - 1)
                    System.out.print(", ");
            }
            System.out.println();
        }
    }

    public static double[][] randomMatrix(int size) {
        double[][] result = new double[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                result[i][j] = random.nextDouble() * 10.0 - 5.0;
            }
        }
        return result;
    }

    public static double[][] mult(double[][] a, double[][] b) throws InterruptedException {
        int w = a[0].length;
        int h = b.length;
        double[][] result = new double[h][w];
        
        Thread even = new Worker(a, b, result, 0, 2);
        even.start();
        
        Thread odd = new Worker(a, b, result, 1, 2);
        odd.start();
        
        even.join();
        odd.join();
        
        return result;
        
    }

    public static void main(String[] args) throws InterruptedException {
        double[][] a = randomMatrix(50);
        double[][] b = randomMatrix(50);
        print(mult(a, b));
    }
}
