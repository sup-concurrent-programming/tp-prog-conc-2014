package tp02;

import java.util.concurrent.ThreadLocalRandom;

public class Customer extends Thread {
    
    private final static int MIN_SLEEP = 500;
    private final static int MAX_SLEEP = 2000;
    private final static int MIN_AMOUNT = -100;
    private final static int MAX_AMOUNT = 100;
    
    int id;
    
    public Customer(int id) {
        this.id = id;
    }
    
    private void deposit(int amount) {
        System.out.format("Customer %d deposit %d\n", id, amount);
        SavingAccount.sum += amount;
        System.out.format("New balance: %d\n", SavingAccount.sum);
    }
    
    private void withdraw(int amount) {
        System.out.format("Customer %d withdraw %d\n", id, amount);
        SavingAccount.sum -= amount;
        System.out.format("New balance: %d\n", SavingAccount.sum);
    }

    @Override
    public void run() {
        while (true) {
            try {
                sleep(ThreadLocalRandom.current().nextInt(MIN_SLEEP, MAX_SLEEP));
                int amount = ThreadLocalRandom.current().nextInt(MIN_AMOUNT, MAX_AMOUNT);
                if (amount < 0) {
                    withdraw(-amount);
                } else if (amount > 0) {
                    deposit(amount);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
