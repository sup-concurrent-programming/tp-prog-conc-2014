package tp02;

public class SavingAccount {
    
    static final int N_OF_CUSTOMERS = 5;
    static long sum = 0;
    
    public static void main(String[] args) {
        for (int i = 0; i < N_OF_CUSTOMERS; i++) {
            Thread t = new Customer(i);
            t.start();
        }
    }

}
