package tp06.concurrent;
import java.util.concurrent.Callable;

import arlut.csd.crypto.Sha512Crypt;

public class Worker implements Callable<Boolean> {
    
    String challenge;
    String guess;
    
    public Worker (String challenge, String guess) {
        this.challenge = challenge;
        this.guess = guess;
    }

    @Override
    public Boolean call() {
        Boolean res = Sha512Crypt.verifyPassword(guess, challenge);
        if (res) {
            System.out.printf("Found: %s\n", guess);
        }
        return res;
    }

}
