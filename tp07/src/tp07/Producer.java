package tp07;

public class Producer extends Person implements Runnable {
	
	public Producer(String name, int frequency) {
		super(name, frequency);
	}
	
	public Producer(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		while (true) {
			CokeMachine.putBottle(this);
			sleep();
		}
	}

}
