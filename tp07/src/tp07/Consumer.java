package tp07;

public class Consumer extends Person implements Runnable {
	
	public Consumer(String name, int frequency) {
		super(name, frequency);
	}
	
	public Consumer(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		while (true) {
			CokeMachine.takeBottle(this);
			sleep();
		}
	}

}
