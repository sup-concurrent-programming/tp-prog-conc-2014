package tp07;

import java.util.Random;

import org.uncommons.maths.random.ExponentialGenerator;
import org.uncommons.maths.random.MersenneTwisterRNG;

public class Person {

	private final static long ONE_MINUTE = 60000;
	private final static int DEFAULT_FREQUENCY = 10;

	private final String name;
	private ExponentialGenerator gen;
	
	public Person(String name, int frequency) {
		this.name = name;
		Random rng = new MersenneTwisterRNG();
		gen = new ExponentialGenerator(frequency, rng);
	}
	
	public Person(String name) {
		this(name, DEFAULT_FREQUENCY);
	}
	
	public Person() {
		this("Person");
	}

	public void sleep() {
		long interval = Math.round(gen.nextValue() * ONE_MINUTE);
		try {
			Thread.sleep(interval);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
	}
	
	@Override
	public String toString() {
		return name;
	}
	
}
