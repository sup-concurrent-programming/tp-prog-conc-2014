package tp07;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CokeMachine {

	static ExecutorService threadExecutor = Executors.newCachedThreadPool();
	final static Lock lock = new ReentrantLock();
	final static Condition notFull = lock.newCondition();
	final static Condition notEmpty = lock.newCondition();
	
	static final int CAPACITY = 10;
	private static final int INITIAL_CAPACITY = 5;
	static int bottles = INITIAL_CAPACITY;
	
	public synchronized static void putBottle(Person p) {
		bottles++;
		System.out.format("++ %1$s puts a bottle in the machine [%2$d bottles]%n", p.toString(), bottles);
	}

	public synchronized static void takeBottle(Person p) {
		bottles--;
		System.out.format("-- %1$s takes a bottle from the machine [%2$d bottles]%n", p.toString(), bottles);
	}
	
	public static void main(String[] args) {
		Person claude = new Producer("Claude");
		Person john = new Producer("John");
		Person alice = new Consumer("Alice");
		Person sarah = new Consumer("Sarah");
		
		threadExecutor.execute((Runnable) claude);
		threadExecutor.execute((Runnable) john);
		threadExecutor.execute((Runnable) alice);
		threadExecutor.execute((Runnable) sarah);
	}

}
